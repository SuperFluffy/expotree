/*
 * Copyright (c) 2012-2014, Gabriel Leventhal, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of the ETH Zurich nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "expo.h"

expo_type* init_expo(expo_type* e) {
  e->N = 0;
  e->ki = 0; 
  e->beta = 0.0;
  e->mu = 0.0;
  e->psi = 0.0;
  e->rho = 0.0;
  e->K = 0.0;
  e->dim = 0;
  e->parVecLen = 0;
  e->curPar = 0;
  e->NVec = NULL;
  e->betaVec = NULL;
  e->muVec = NULL;
  e->psiVec = NULL;
  e->gamma = 0.0;
  e->shift = 0.0;
  e->vflag = 0;
  e->rescale = 1;
  e->tol = 0.0;
  e->cutoff = 0.0;
  e->SImodel = 1;
  e->lambda = &lambdaSI;
  e->offset = 0;
  e->useLog = 0;
  e->includeZero = 0;
  e->mat_inf_norm = 0.0;
  e->mat_one_norm = 0.0;
  e->mat = NULL;
  e->mat_i = NULL;
  e->mat_n = 0;
  e->matvec = &matFuncExpmv;
  e->lambdaVec = NULL;
  e->muFun = NULL;
  e->psiFun = NULL;
  e->norm = &matFuncOneNorm;
  e->trace = &matFuncTrace;
  e->ft = &transEvent;
  e->fs = &sampleEvent;
  e->fs2 = &sampleEventNoShift;
  e->p_max = 8;  // +1 than used in the loop calculation
  e->m_max = 55; // +1 than used in the loop calculation
  e->N_max = 0;
  e->est_norm = 0;
  e->init_all = &init_all;
  e->p_save = NULL;
  e->init_all = &init_all;
  e->user_funcs = 0;
  return e;
}

/****************************************************************************/

expo_type* expo_type_alloc() {
  expo_type* expo = (expo_type*) malloc(sizeof(expo_type));
  return init_expo(expo);
}

void expo_type_free(expo_type* expo) {
  free(expo);
  expo = NULL;
}

/************************************************************/ 
/* Initialte matrix                                         */
/************************************************************/ 

void init_mat(expo_type* expo) {
  int m;
  int N = expo->N_max+1;
  for (m = 0; m < N; ++m) {
    if (m < expo->ki) {
      expo->mat[m] = 0.0;
      expo->mat[m+N] = 0.0;
      expo->mat[m+2*N] = 0.0;
    } else {
      /* lower diagonal */
      expo->mat[m]     = (m > expo->ki) ? (m-expo->ki)*expo->mu : 0.0;
      /*    diagonal    */
      expo->mat[N+m]   = -m*(expo->lambdaVec[m]+expo->psi+expo->mu) - expo->shift;
      /* upper diagonal */
      expo->mat[2*N+m] = (m < expo->N) ? (m+expo->ki)*expo->lambdaVec[m] : 0.0;
    }
  }
}

/************************************************************/ 
/* Initialte lambda vector                                  */
/************************************************************/ 

void init_lambda(expo_type* expo) {
  int m;
  for (m = 0; m <= expo->N_max; ++m) {
    expo->lambdaVec[m] = expo->lambda(m,expo);
  }
}

void init_all(expo_type* expo) {
  init_lambda(expo);
  expo->shift = expo->trace(expo)/expo->dim;  /* shift matrix */
  init_mat(expo);
};

/************************************************************/ 
/* Lambda functions                                         */
/************************************************************/ 

double lambdaSI(int I, expo_type* expo) { 
  if (I > 0) {
    double l = expo->beta*(1.-I/expo->K);
    return (l > 0.0) ? l : 0.0;
  } else {
    return 0.0;
  }
}

/************************************************************/ 

double lambdaInf(int I, expo_type* expo) { 
  if (I > 0 && I <= expo->N) 
    return expo->beta;
  else 
    return 0.0;
}

/************************************************************/ 
/* Column and row sums                                      */
/************************************************************/ 

double matRowSum(int m, expo_type* expo) {
  int N = expo->N_max+1;
  const double *ld = expo->mat;
  const double *d  = ld + N;
  const double *ud = d + N;
  if (m >= 0 && m < N) return ld[m] + d[m] + ud[m];
  else return 0.0;
}

/************************************************************/ 

double matColSum(int m, expo_type* expo) {
  int N = expo->N_max+1;
  const double *ld = expo->mat;
  const double *d  = ld + N;
  const double *ud = d + N;
  double cs = 0.0;
  if (m >= expo->ki && m < N) {
    cs = d[m];
    if (m > 0) cs += ud[m-1];
    if (m < expo->N_max) cs += ld[m+1];
  }
  return cs;
}

/************************************************************/ 
/* Matrix-vector product required for EXPMV                 */
/* - matrix-matrix product                                  */
/* - shifted matrix-matrix product                          */
/* - shifted matrix norm                                    */
/* - matrix trace                                           */
/************************************************************/ 

void matFuncExpmv(char trans, int n1, int n2, 
                  double alpha, double* pin, double* pout,
                  void* pars) 
{
  expo_type* expo = (expo_type*) pars;
  int N = expo->N_max+1;

#if defined(DLAGTM)
  double done  = 1.0;
  double dzero = 0.0;
  int ione = 1;
  dlagtm_(&trans,&N,&n2,&done,
          expo->mat+1,expo->mat+N,expo->mat+2*N,pin, 
          &N,&dzero,pout,&N);
  dscal_(&N,&alpha,pout,&ione);

#elif defined(DDIAGEMV)
  char transa = 'n';
  int idiag[] = { -1, 0, 1 };
  int ndiag = 3;
  int ione = 1;
  mkl_ddiagemv(&transa,&N,expo->mat,&N,idiag,&ndiag,pin,pout);
  dscal_(&N,&alpha,pout,&ione);

#else
  double aa;
  double bb;
  double cc;
  int m;
  for (m = 0; m < expo->ki; ++m) pout[m] = 0.0;
  for (m = expo->ki; m < N; ++m) {
    cc = expo->mat[m]*pin[m-1];
    aa = expo->mat[N+m]*pin[m];
    bb = expo->mat[2*N+m]*pin[m+1];
    pout[m] = alpha*(aa+bb+cc);
    // THROW EXCEPTION ???
    // if (isnan(pout[m])) pout[m] = 0.0;
  }
#endif
}

/************************************************************/ 

double matFuncTrace(void* pars) {
  expo_type* expo = (expo_type*) pars;
  double trace = 0.0;
  int m = 0;
  for (m = expo->ki; m < expo->N_max+1; ++m) {
    trace += -m*(expo->lambdaVec[m]+expo->psi+expo->mu);
  }
  return trace;
}

/************************************************************/ 

double matFuncOneNorm(void* pars) {
  expo_type* expo = (expo_type*) pars;
  char norm = 'o';
  int N = expo->N_max+1;
  return dlangt_(&norm,&N,expo->mat+1,expo->mat+N,expo->mat+2*N);
}

/************************************************************/ 

double matFuncInfNorm(void* pars) {
  expo_type* expo = (expo_type*) pars;
  char norm = 'i';
  int N = expo->N_max+1;
  return dlangt_(&norm,&N,expo->mat+1,expo->mat+N,expo->mat+2*N);
}

/************************************************************/ 

double matFuncOneNormAnalytical(void* pars) {
  expo_type* expo = (expo_type*) pars;
  double maxM = .5*expo->N*(1.+(expo->mu+.5*expo->psi)/expo->beta)
                - .5 - .25*expo->ki;
  int maxMi = rint(maxM);
  if (maxMi < expo->ki) maxMi = expo->ki;
  if (maxMi > expo->N) maxMi = expo->N;
  return matColSum(maxMi,expo);
}

/************************************************************/ 

double matFuncInfNormAnalytical(void* pars) {
  expo_type* expo = (expo_type*) pars;
  double maxRow = .5*expo->N + .25*expo->ki 
                  + .25*expo->N/expo->beta*(expo->mu+expo->psi);
  int maxRi = rint(maxRow);
  if (maxRi < expo->ki) maxRi = expo->ki;
  if (maxRi > expo->N) maxRi = expo->N;
  return matRowSum(maxRi,expo);
}

/************************************************************/ 

void transEvent(double* pin, double* pout, expo_type* expo) {
  int m;
  for (m = 0; m < expo->N_max; ++m) {
    pout[m] = 2.0*expo->lambdaVec[m]*pin[m+1];
  }
  pout[expo->N_max] = 0.0;
}

/************************************************************/ 

void sampleEvent(double* pin, double* pout, expo_type* expo) {
  int m;
  pout[0] = 0.0;
  for (m = 1; m <= expo->N_max; ++m) {
    pout[m] = expo->psi*pin[m-1];
  }
  // pout[expo->N_max] = 0.0;
}

/************************************************************/ 

void sampleEventNoShift(double* pin, double* pout, 
    expo_type* expo) {
  int m;

  for (m = 0; m < expo->ki; ++m) 
    pout[m] = 0.0;

  for (m = expo->ki; m <= expo->N_max; ++m) 
    pout[m] = expo->psi*pin[m];
}

/************************************************************/ 

int max_pop_size(const expo_type* expo) {
  /* get maximum carrying capacity */
  int N = (int) ceil(expo->NVec[0]);
  int N2 = 0;
  int j;
  for (j = 0; j < expo->parVecLen; ++j) {
    N2 = (int) ceil(expo->NVec[j]);
    if (N < N2) N = N2;
  }
  return N;
}

/************************************************************/ 

