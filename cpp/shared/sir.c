/*
 * Copyright (c) 2012-2014, Gabriel Leventhal, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of the ETH Zurich nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "sir.h"

/****************************************************************************/

int sir_index(int I, int R, const expo_type* pars) {
  return R*(pars->N_max+1) - R*(R-1)/2 + I;
}

int sir_index_N(int I, int R, int N) {
  return R*(N+1) - R*(R-1)/2 + I;
}

/****************************************************************************/

void sir_init_all(expo_type* expo) {
  sir_init_lambda(expo);
  expo->shift = sir_trace(expo)/expo->dim;
  // expo->shift = 0.0;
  sir_init_mat(expo);
}

/****************************************************************************/

void sir_init_lambda(expo_type* expo) {
  double S;
  int I, R;
  int m = 0;
  for (R = 0; R <= expo->N_max; ++R) {
    for (I = 0; I <= expo->N_max-R; ++I) {
      S = expo->K - R - I;
      if (S < 0.0) S = 0.0;
      expo->lambdaVec[m++] = expo->beta*S/expo->N_max;
    }
  }
}

/****************************************************************************/

void sir_init_mat(expo_type* expo) {
  const int dim = sir_index(0,expo->N_max,expo)+1;
  double S;
  int I, R;
  int m = 0;
  int j;
  memset(expo->mat,0,3*dim*sizeof(double));
  for (R = 0; R <= expo->N_max; ++R) {
    for (I = expo->ki; I <= expo->N_max-R; ++I) {
      S = expo->K - R - I;
      if (S < 0.0) S = 0.0;

      j = sir_index(I,R,expo);
      expo->mat[m]   = -I*(expo->lambdaVec[j]+expo->mu+expo->psi) - expo->shift;
      expo->mat_i[m] = j;

      if (R < expo->N_max) {
        expo->mat[dim+m] = (I-expo->ki)*expo->mu;
        expo->mat_i[dim+m] = sir_index(I-1,R+1,expo);
      } else {
        expo->mat[dim+m] = 0.0;
        expo->mat_i[dim+m] = 0;
      }

      if (I+R < expo->N_max) {
        expo->mat[2*dim+m] = (I+expo->ki)*expo->lambdaVec[m];
        expo->mat_i[2*dim+m] = sir_index(I+1,R,expo);
      } else {
        expo->mat[2*dim+m] = 0.0;
        expo->mat_i[2*dim+m] = 0;
      }

      ++m;
    }
  }
  expo->mat_n = m;
}

/****************************************************************************/

void sir_matfunc(char trans, int n1, int n2, 
                 double alpha, double* pin, double* pout,
                 void* pars) 
{
  expo_type* expo = (expo_type*) pars;
  int dim = sir_index(0,expo->N_max,expo)+1;
  int m;
  int j;
  memset(pout,0,expo->dim*sizeof(double));
  for (m = 0; m < expo->mat_n; ++m) {
    j = expo->mat_i[m];
    pout[j]  = expo->mat[m]       * pin[j];
    pout[j] += expo->mat[dim+m]   * pin[expo->mat_i[dim+m]];
    pout[j] += expo->mat[2*dim+m] * pin[expo->mat_i[2*dim+m]];
    pout[j] *= alpha;
  }
}

/****************************************************************************/

double sir_trace(void* pars) {
  expo_type* expo = (expo_type*) pars;
  double trace = 0.0;
  int m = 0;
  int I, R;
  for (R = 0; R <= expo->N_max; ++R) {
    for (I = expo->ki; I <= expo->N_max-R; ++I) {
      // m = sir_index(I,R,expo);
      trace += -I*(expo->lambdaVec[m]+expo->mu+expo->psi);
    }
  }
  return trace;
}

/****************************************************************************/

double sir_inf_norm(void* pars) {
  expo_type* expo = (expo_type*) pars;
  int m = 0;
  double rsum = 0.0;
  double maxrsum = 0.0;
  int dim = sir_index(0,expo->N_max,expo)+1;
  for (m = 0; m < expo->mat_n; ++m) {
    rsum  = fabs(expo->mat[m]);
    rsum += fabs(expo->mat[dim+m]);
    rsum += fabs(expo->mat[2*dim+m]);
    if (rsum > maxrsum) maxrsum = rsum;
  }
  return maxrsum;
}

/****************************************************************************/

double sir_one_norm(void* pars) {
  expo_type* expo = (expo_type*) pars;
  int dim = sir_index(0,expo->N_max,expo)+1;
  double* csum = (double*) calloc(dim,sizeof(double));
  int m = 0;
  for (m = 0; m < expo->mat_n; ++m) {
    csum[expo->mat_i[m]] += fabs(expo->mat[m]);
    csum[expo->mat_i[dim+m]] += fabs(expo->mat[dim+m]);
    csum[expo->mat_i[2*dim+m]] += fabs(expo->mat[2*dim+m]);
  }
  double maxcsum = 0.0;
  for (m = 0; m < dim; ++m) {
    if (csum[m] > maxcsum) maxcsum = csum[m];
  }

  free(csum);
  return maxcsum;
}

/****************************************************************************/

void sir_trans(double* pin, double* pout, expo_type* expo) {
  int I, R;
  int m = 0;
  int n;
  for (R = 0; R <= expo->N_max; ++R) {
    for (I = 0; I < expo->N_max-R; ++I) {
      n = sir_index(I+1,R,expo);
      m = sir_index(I,R,expo);
      pout[m] = 2.0*expo->lambdaVec[m]*pin[n];
      ++m;
    }
    pout[m++] = 0.0; /* I = N-R */
  }
}

/************************************************************/ 

void sir_sample(double* pin, double* pout, expo_type* expo) {
  int I, R;
  int m = 0;
  int n;
  for (R = 0; R < expo->N_max; ++R) {
    pout[m++] = 0.0; /* I = 0 */
    for (I = 1; I <= expo->N_max-R; ++I) {
      n = sir_index(I-1,R+1,expo);
      pout[m] = expo->psi*pin[n];
      ++m;
    }
  }
  /* R = N, I = 0 */
  pout[m++] = 0.0;
}

/************************************************************/ 

void sir_find_nonzero(double* p, expo_type* expo) {
  int I, R;
  int m = 0;
  for (R = 0; R <= expo->N_max; ++R) {
    for (I = 0; I <= expo->N_max-R; ++I) {
      if (p[m] != 0.0) printf("p[%d,%d] = %g\n",I,R,p[m]);
      ++m;
    }
  }
}


