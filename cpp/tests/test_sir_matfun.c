#include <stdlib.h>

#include "shared/sir.h"

int main() {
  expo_type* expo = (expo_type*) malloc(sizeof(expo_type));
  init_expo(expo);

  expo->N_max = 10;
  expo->K = 10.0;
  expo->beta = 1.0;
  expo->mu = 0.2;
  expo->psi = 0.2;
  expo->ki = 0;

  int dim = sir_index(0,expo->N_max,expo)+1;
  expo->dim = dim;
  printf("dim = %d\n",dim);

  expo->lambdaVec = (double*) malloc(dim*sizeof(double));
  expo->mat = (double*) malloc(3*dim*sizeof(double));
  expo->mat_i = (int*) malloc(3*dim*sizeof(int));

  double* pin  = (double*) calloc(dim,sizeof(double));
  double* pout = (double*) calloc(dim,sizeof(double));

  int m = sir_index(0,10,expo);
  pin[m] = 1.0;

  double trace = 0.0;
  double trace2 = 0.0;
  double shift = 0.0;

  sir_init_lambda(expo);

  trace = sir_trace(expo); 
  printf("Matrix trace = %12g\n",trace);

  shift = trace/expo->dim; 
  printf("Matrix shift = %12g\n",shift);

  sir_init_all(expo);

  trace2 = 0.0;
  for (m = 0; m < expo->mat_n; ++m) trace2 += expo->mat[m];
  printf("Shifted trace = %g\n",trace2);

  double one_norm = sir_one_norm(expo);
  printf("  1-norm = %g\n",one_norm);

  double inf_norm = sir_inf_norm(expo);
  printf("Inf-norm = %g\n",inf_norm);

  sir_init_all(expo);

  sir_matfunc('n',0,0,1.0,pin,pout,expo);

  for (m = 0; m < expo->mat_n; ++m) {
    printf("%2d || %2d:% 6.2f | %2d:% 6.2f | %2d:% 6.2f\n",
           m,expo->mat_i[m],expo->mat[m],
           expo->mat_i[dim+m],expo->mat[dim+m],
           expo->mat_i[2*dim+m],expo->mat[2*dim+m]);
  }


  m = 0;
  int I, R;
  for (R = 0; R <= expo->N_max; ++R) {
    for (I = 0; I <= expo->N_max-R; ++I) {
      printf("p(%2d|%2d,%2d) = %g\n",m,I,R,pout[m]);
      ++m;
    }
  }


  free(pin);
  free(pout);

  free(expo->lambdaVec);
  free(expo->mat_i);
  free(expo->mat);

  return 0;
}


